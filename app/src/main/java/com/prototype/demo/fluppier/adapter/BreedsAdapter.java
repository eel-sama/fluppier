package com.prototype.demo.fluppier.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prototype.demo.fluppier.R;
import com.prototype.demo.fluppier.objects.Dog;
import com.prototype.demo.fluppier.viewholder.BreedViewHolder;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BreedsAdapter extends RecyclerView.Adapter<BreedViewHolder> {

    @Inject
    Picasso picasso;

    Context context;

    ArrayList<Dog> dogs = new ArrayList<>();

    public @Inject
    BreedsAdapter(@Named("application_context") Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public BreedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.breeds_recyclerview_item, parent, false);
        BreedViewHolder breedViewHolder = new BreedViewHolder(view);
        breedViewHolder.setIsRecyclable(false);
        return breedViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BreedViewHolder holder, int position) {
        holder.breed.setText(StringUtils.capitalize(dogs.get(position).getBreed()));
        picasso.load(dogs.get(position).getImg())
                .error(R.drawable.na)
                .placeholder(R.drawable.progress_animation)
                .resize(500, 350)
                .centerCrop().into(holder.img);
    }

    @Override
    public int getItemCount() {
        return dogs.size();
    }

    public void setItems(Dog dog){
        this.dogs.add(dog);
        notifyDataSetChanged();
    }
}
