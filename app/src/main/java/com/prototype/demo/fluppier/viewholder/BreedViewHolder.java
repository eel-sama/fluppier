package com.prototype.demo.fluppier.viewholder;

import android.view.View;
import android.widget.TextView;

import com.prototype.demo.fluppier.R;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

public class BreedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView breed;
    public AppCompatImageView img;

    public BreedViewHolder(@NonNull View itemView) {
        super(itemView);
        breed = itemView.findViewById(R.id.breed);
        img = itemView.findViewById(R.id.breed_image);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

    }
}
