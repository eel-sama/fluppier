package com.prototype.demo.fluppier.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.prototype.demo.fluppier.model.MainActivityModel;
import com.prototype.demo.fluppier.objects.Dog;
import com.prototype.demo.fluppier.objects.Img;
import com.prototype.demo.fluppier.view.MainActivityView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivityPresenterImpl implements MainActivityPresenter {

    MainActivityView mainActivityView;
    MainActivityModel mainActivityModel;

    public @Inject
    MainActivityPresenterImpl(MainActivityView mainActivityView, MainActivityModel mainActivityModel) {
        this.mainActivityView = mainActivityView;
        this.mainActivityModel = mainActivityModel;
    }

    @Override
    public void getBreedList() {
        mainActivityView.showProgressbar();
        mainActivityModel.getBreedResponse(this);
    }

    @Override
    public void onResponse(Retrofit retrofit, Response<?> response) {
        if (response.isSuccessful()) {
            try {
                JSONObject json = new JSONObject(response.body().toString());
                Map<String, ArrayList<String>> retMap = new Gson().fromJson(
                        json.getString("message"), new TypeToken<HashMap<String, Object>>() {}.getType()
                );

                for(Map.Entry<String, ArrayList<String>> entry: retMap.entrySet()){
                    if(entry.getValue().size() == 0){
                        mainActivityModel.getBreedImg(this,entry.getKey());
                    }else {
                        for(String s: entry.getValue()){
                            mainActivityModel.getBreedImg(this,entry.getKey()+"-"+s);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onFail() {
        mainActivityView.hideProgressbar();
        mainActivityView.noInternetConnection();
    }

    @Override
    public void onBreedGetImg(Response<?> response, String breed) {
        if(response.isSuccessful()){
            Img img = (Img)response.body();
            Dog dog = new Dog(breed,img.getMessage());
            mainActivityView.onLoadBreed(dog);
        }
        mainActivityView.hideProgressbar();
    }
}


