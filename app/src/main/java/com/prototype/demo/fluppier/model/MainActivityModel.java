package com.prototype.demo.fluppier.model;

import android.util.Log;

import com.google.gson.JsonObject;
import com.prototype.demo.fluppier.api.DogApi;
import com.prototype.demo.fluppier.listener.BreedListListener;
import com.prototype.demo.fluppier.objects.Img;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivityModel {

    @Inject
    DogApi dogApi;

    @Inject
    Retrofit retrofit;

    public @Inject
    MainActivityModel() {

    }

    public void getBreedResponse(final BreedListListener breedListListener) {
        final Call<JsonObject> dogCall = dogApi.getBreeds();
        dogCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                breedListListener.onResponse(retrofit, response);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                breedListListener.onFail();
            }
        });
    }

    public void getBreedImg(final BreedListListener breedListListener, final String breed) {
        Call<Img> imgCall = dogApi.getBreedImg(breed);

        imgCall.enqueue(new Callback<Img>() {
            @Override
            public void onResponse(Call<Img> call, Response<Img> response) {
                breedListListener.onBreedGetImg(response,breed);
            }

            @Override
            public void onFailure(Call<Img> call, Throwable t) {
                Log.e("Fail",""+t.getMessage());
                breedListListener.onFail();
            }
        });

    }
}
