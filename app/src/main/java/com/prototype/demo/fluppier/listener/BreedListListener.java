package com.prototype.demo.fluppier.listener;

import com.prototype.demo.fluppier.api.DogApi;

import retrofit2.Response;
import retrofit2.Retrofit;

public interface BreedListListener {

    public void onResponse(Retrofit retrofit, Response<?> response);

    public void onFail();

    public void onBreedGetImg(Response<?> response, String breed);
}
