package com.prototype.demo.fluppier.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    @Named("application_context")
    @Singleton
    @Provides
    public Context context(Application application) {
        return application.getApplicationContext();
    }
}
