package com.prototype.demo.fluppier.view;

import com.prototype.demo.fluppier.objects.Dog;

public interface MainActivityView {
    public void onLoadBreed(Dog dog);

    public void noInternetConnection();

    public void showProgressbar();

    public void hideProgressbar();
}
