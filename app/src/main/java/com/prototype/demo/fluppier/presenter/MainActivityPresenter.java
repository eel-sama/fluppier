package com.prototype.demo.fluppier.presenter;

import com.prototype.demo.fluppier.listener.BreedListListener;

public interface MainActivityPresenter extends BreedListListener {
    void getBreedList();
}

