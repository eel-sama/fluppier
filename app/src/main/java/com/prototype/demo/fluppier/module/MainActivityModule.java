package com.prototype.demo.fluppier.module;

import com.prototype.demo.fluppier.MainActivity;
import com.prototype.demo.fluppier.model.MainActivityModel;
import com.prototype.demo.fluppier.presenter.MainActivityPresenter;
import com.prototype.demo.fluppier.presenter.MainActivityPresenterImpl;
import com.prototype.demo.fluppier.view.MainActivityView;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class MainActivityModule {

    @Provides
    static MainActivityPresenter provideMainActivityPresenter(MainActivityView mainActivityView, MainActivityModel mainActivityModel){
        return new MainActivityPresenterImpl(mainActivityView, mainActivityModel);
    }

    @Binds
    abstract MainActivityView provideMainActivityView(MainActivity mainActivity);
}
