package com.prototype.demo.fluppier;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.prototype.demo.fluppier.adapter.BreedsAdapter;
import com.prototype.demo.fluppier.objects.Dog;
import com.prototype.demo.fluppier.presenter.MainActivityPresenter;
import com.prototype.demo.fluppier.view.MainActivityView;

import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements MainActivityView {

    @Inject
    MainActivityPresenter mainActivityPresenter;

    @Inject
    BreedsAdapter breedsAdapter;

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    ProgressBar mprogressBar;
    CoordinatorLayout coordinatorLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.breed_list_recyclerview);
        coordinatorLayout = findViewById(R.id.mainactivity_coordinatorlayout);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mprogressBar = findViewById(R.id.progressBar);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.paw);
        getSupportActionBar().setTitle("");
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainActivityPresenter.getBreedList();
    }

    @Override
    public void onLoadBreed(Dog dog) {
        breedsAdapter.setItems(dog);
        recyclerView.setAdapter(breedsAdapter);
    }

    @Override
    public void noInternetConnection() {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "No internet connection!", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

        snackbar.setActionTextColor(Color.BLACK);

        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        TextView textView = sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }

    @Override
    public void showProgressbar() {
        recyclerView.setVisibility(View.GONE);
        mprogressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressbar() {
        recyclerView.setVisibility(View.VISIBLE);
        mprogressBar.setVisibility(View.GONE);
    }
}
