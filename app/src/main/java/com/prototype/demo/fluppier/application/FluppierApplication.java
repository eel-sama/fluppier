package com.prototype.demo.fluppier.application;

import com.prototype.demo.fluppier.component.DaggerFluppierComponent;
import com.prototype.demo.fluppier.component.FluppierComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class FluppierApplication extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        FluppierComponent fluppierComponent = DaggerFluppierComponent.builder().application(this).build();
        fluppierComponent.inject(this);
        return fluppierComponent;
    }
}
