package com.prototype.demo.fluppier.component;

import android.app.Application;

import com.prototype.demo.fluppier.api.DogApi;
import com.prototype.demo.fluppier.application.FluppierApplication;
import com.prototype.demo.fluppier.module.ActivityBuilderModule;
import com.prototype.demo.fluppier.module.FluppierModule;
import com.prototype.demo.fluppier.module.PicassoModule;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {FluppierModule.class, PicassoModule.class, ActivityBuilderModule.class, AndroidSupportInjectionModule.class})
public interface FluppierComponent extends AndroidInjector<DaggerApplication> {

    void inject (FluppierApplication app);

    @Override
    void inject (DaggerApplication instance);

    @Component.Builder
    interface Builder{
        @BindsInstance
        Builder application(Application application);

        FluppierComponent build();
    }

}
