package com.prototype.demo.fluppier.api;

import com.google.gson.JsonObject;
import com.prototype.demo.fluppier.objects.Dog;
import com.prototype.demo.fluppier.objects.Img;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DogApi {

    @GET("breed/{breed}/images/random/6")
    Call<Dog> getBreedImgs(@Path("breed") String breed);

    @GET("breed/{breed}/images/random")
    Call<Img> getBreedImg(@Path("breed") String breed);

    @GET("breeds/list/all")
    Call<JsonObject> getBreeds();
}
